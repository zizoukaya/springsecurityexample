package com.example;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DemoApplication.class)
@ContextConfiguration(classes = {DemoApplication.class, MvcConfig.class, WebSecurityConfig.class})
@WebAppConfiguration
public class DemoApplicationTests {
	
 	@Autowired
    private WebApplicationContext context;
    private MockMvc mvc;
    
	@Before
	public void setup() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
		SecurityContextHolder.clearContext();
	}
	@Test
	public void allowsAccessToRootResource() throws Exception {

		mvc.perform(get("/")).
				andExpect(status().isOk()).
				andDo(print());

		mvc.perform(get("/home")).
				andExpect(status().isOk()).
				andDo(print());

		mvc.perform(get("/login")).
				andExpect(status().isOk()).
				andDo(print());
	}
}
